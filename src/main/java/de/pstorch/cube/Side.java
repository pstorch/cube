package de.pstorch.cube;

import java.util.HashMap;
import java.util.Map;

public enum Side {

    FRONT, BACK, TOP, BOTTOM, LEFT, RIGHT;

    static Map<Side, Side> OPPOSITES = new HashMap<>();

    static {
        OPPOSITES.put(FRONT, BACK);
        OPPOSITES.put(BACK, FRONT);
        OPPOSITES.put(BOTTOM, TOP);
        OPPOSITES.put(TOP, BOTTOM);
        OPPOSITES.put(LEFT, RIGHT);
        OPPOSITES.put(RIGHT, LEFT);
    }

    public Side opposite() {
        return OPPOSITES.get(this);
    }

}
